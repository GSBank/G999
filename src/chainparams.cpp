// Copyright (c) 2010 Satoshi Nakamoto
// Copyright (c) 2009-2014 The Bitcoin developers
// Copyright (c) 2014-2015 The Dash developers
// Copyright (c) 2015-2018 The PIVX developers
// Copyright (c) 2018-2020 The G999 Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "chainparams.h"

#include "random.h"
#include "util.h"
#include "utilstrencodings.h"

#include <assert.h>

#include <boost/assign/list_of.hpp>

using namespace std;
using namespace boost::assign;

struct SeedSpec6 {
    uint8_t addr[16];
    uint16_t port;
};

#include "chainparamsseeds.h"

/**
 * Main network
 */


static void convertSeed6(std::vector<CAddress>& vSeedsOut, const SeedSpec6* data, unsigned int count)
{
    const int64_t nOneWeek = 7 * 24 * 60 * 60;
    for (unsigned int i = 0; i < count; i++) {
        struct in6_addr ip;
        memcpy(&ip, data[i].addr, sizeof(ip));
        CAddress addr(CService(ip, data[i].port));
        addr.nTime = GetTime() - GetRand(nOneWeek) - nOneWeek;
        vSeedsOut.push_back(addr);
    }
}

static Checkpoints::MapCheckpoints mapCheckpoints =
    boost::assign::map_list_of
    (0, uint256("000007b32b028d4a44facc84993a4b4b18a038651becedb5a26bb69183f27488"));	
static const Checkpoints::CCheckpointData data = {
    &mapCheckpoints,
    1598616000,   // * UNIX timestamp of last checkpoint block
    0,        // * total number of transactions between genesis and last checkpoint (the tx=... number in the SetBestChain debug.log lines)
    5          // * estimated number of transactions per day after checkpoint
};

static Checkpoints::MapCheckpoints mapCheckpointsTestnet =
    boost::assign::map_list_of
    (0, uint256("000009997ec1c62ee220373ba6e563121dadd3e465cd01672a4312e4864fcc81"));
static const Checkpoints::CCheckpointData dataTestnet = {
    &mapCheckpointsTestnet,
    1598616000,
    0,
    5};

static Checkpoints::MapCheckpoints mapCheckpointsRegtest =
    boost::assign::map_list_of(0, uint256("0x001"));
static const Checkpoints::CCheckpointData dataRegtest = {
    &mapCheckpointsRegtest,
    1598616000,
    0,
    5};

class CMainParams : public CChainParams
{
public:
    CMainParams()
    {
        networkID = CBaseChainParams::MAIN;
        strNetworkID = "main";
        pchMessageStart[0] = 0x9a;
        pchMessageStart[1] = 0x3f;
        pchMessageStart[2] = 0xbb;
        pchMessageStart[3] = 0x21;
        vAlertPubKey = ParseHex("047746b7a7e7b79c7c3f81bbe4d38bcdf525ed74b6ac4353e66fc495cf011daf2cf9bae78aea20eda642067a8083c096386f5cdad1f980abf1bd74d60edb7260c8");
        nDefaultPort = 33999;
        bnProofOfWorkLimit = ~uint256(0) >> 20; // G999 starting difficulty is 1 / 2^12
        nSubsidyHalvingInterval = 525600; // one year
        nMaxReorganizationDepth = 100;
        nEnforceBlockUpgradeMajority = 750;
        nRejectBlockOutdatedMajority = 950;
        nToCheckBlockUpgradeMajority = 1000;
        nMinerThreads = 0;
        nTargetTimespan = 1 * 60; // G999: every block
        nTargetSpacing = 1 * 60;  // G999: 1 minute
        nLastPOWBlock = 250;
        nMaturity = 100;
        nCollateralMaturity = 525600; // one year
        nCollateralMaturityEnforcementHeight = 70000;
        nCollateralMaturityTimeWindow = 60 * 24 * 10; // 10 days
        nMasternodeMaxCount = 4000;		// maximum number of masternodes allowed
        nMasternodeCountDrift = 20;
        nModifierUpdateBlock = 1;
        nMaxMoneyOut = 92000000000 * COIN;
        nMasternodeCollateral = 749999; 
        nStakeMinConfirmations = 300;   // Required number of confirmations
        nStakeMinAmount = 249999 * COIN;   // Minimum required staking amount

        const char* pszTimestamp = "Gold Standard Bank";
        CMutableTransaction txNew;
        txNew.vin.resize(1);
        txNew.vout.resize(1);
        txNew.vin[0].scriptSig = CScript() << 486604799 << CScriptNum(4) << vector<unsigned char>((const unsigned char*)pszTimestamp, (const unsigned char*)pszTimestamp + strlen(pszTimestamp));
        txNew.vout[0].nValue = 50 * COIN;
        txNew.vout[0].scriptPubKey = CScript() << ParseHex("044fe9e75b1b8dae3574234f03c96d6cbd8b06ff4103332c084d0d860dc384c3470ada4cbcd4b44a6852f4a17c219dcf0558da5e3f82d1e8e5a27b9eb83daa8661") << OP_CHECKSIG;
        genesis.vtx.push_back(txNew);
        genesis.hashPrevBlock = 0;
        genesis.hashMerkleRoot = genesis.BuildMerkleTree();
        genesis.nVersion = 1;
        genesis.nTime = 1598616000;
        genesis.nBits = 0x1e0fffff;
        genesis.nNonce = 3656940;

        hashGenesisBlock = genesis.GetHash();
        assert(hashGenesisBlock == uint256("0x000007b32b028d4a44facc84993a4b4b18a038651becedb5a26bb69183f27488"));
        assert(genesis.hashMerkleRoot == uint256("0xbc9ff7120e6fcac55601f23fa00f0b7159a8f5c3f666f49425b99b2f1a387f84"));

        vSeeds.push_back(CDNSSeedData("seed1", "seed1.g999.io"));
        vSeeds.push_back(CDNSSeedData("seed2", "seed2.g999.io"));
        vSeeds.push_back(CDNSSeedData("seed3", "seed3.g999.io"));
        vSeeds.push_back(CDNSSeedData("seed4", "seed4.g999.io"));
        vSeeds.push_back(CDNSSeedData("seed5", "seed5.g999.io"));
        vSeeds.push_back(CDNSSeedData("seed6", "seed6.g999.io"));		
        
        base58Prefixes[PUBKEY_ADDRESS] = std::vector<unsigned char>(1, 38); // G999 addresses start with 'G'
        base58Prefixes[SCRIPT_ADDRESS] = std::vector<unsigned char>(1, 18); // G999 script addresses start with '8'
        base58Prefixes[SECRET_KEY] = std::vector<unsigned char>(1, 158);    // G999 private keys start with 'Q'
        // G999 BIP32 pubkeys start with 'xpub' (Bitcoin defaults)
        base58Prefixes[EXT_PUBLIC_KEY] = boost::assign::list_of(0x04)(0x88)(0xB2)(0x1E).convert_to_container<std::vector<unsigned char> >();
        // G999 BIP32 pubkeys start with 'xprv' (Bitcoin defaults)		
        base58Prefixes[EXT_SECRET_KEY] = boost::assign::list_of(0x04)(0x88)(0xAD)(0xE4).convert_to_container<std::vector<unsigned char> >();
        // 	BIP44 coin type is '0X8000028a' from https://github.com/satoshilabs/slips/blob/master/slip-0044.md
        base58Prefixes[EXT_COIN_TYPE] = boost::assign::list_of(0x80)(0x00)(0x02)(0x8a).convert_to_container<std::vector<unsigned char> >();

        convertSeed6(vFixedSeeds, pnSeed6_main, ARRAYLEN(pnSeed6_main));

        fRequireRPCPassword = true;
        fMiningRequiresPeers = true;
        fAllowMinDifficultyBlocks = false;
        fDefaultConsistencyChecks = false;
        fRequireStandard = true;
        fMineBlocksOnDemand = false;
        fSkipProofOfWorkCheck = false;
        fTestnetToBeDeprecatedFieldRPC = false;
        fHeadersFirstSyncingActive = false;

        nPoolMaxTransactions = 3;
        strSporkKey = "04a9f0885c8b38308e8b828e0b44be1d495249c1007876b44ba043b44d10a9d2603aeacc7aabdecc92862caa15ea5f730e5d583fafda6097ef1ae490742339189d";
        strMasternodesendPoolDummyAddress = "GVMymvtExa1qnQ7gZzuYG73R4hF421oQTG";
        nStartMasternodePayments = 1598616000;
    }

    const Checkpoints::CCheckpointData& Checkpoints() const
    {
        return data;
    }
};
static CMainParams mainParams;

/**
 * Testnet (v3)
 */
class CTestNetParams : public CMainParams
{
public:
    CTestNetParams()
    {
        networkID = CBaseChainParams::TESTNET;
        strNetworkID = "test";
        pchMessageStart[0] = 0xe3;
        pchMessageStart[1] = 0x82;
        pchMessageStart[2] = 0xaf;
        pchMessageStart[3] = 0x99;
        vAlertPubKey = ParseHex("04f3a181728d69a65b790fd5e935cfdc4a81b7eb433be4490c4602b59300814c70056fba5a0256d28656f456848a3430f996df96340c4b5275f8f6013c8616dfb6");
        nDefaultPort = 33997;
        nEnforceBlockUpgradeMajority = 51;
        nRejectBlockOutdatedMajority = 75;
        nToCheckBlockUpgradeMajority = 100;
        nMinerThreads = 0;
        nTargetTimespan = 1 * 60; // G999: 1 day
        nTargetSpacing = 1 * 60;  // G999: 1 minute
        nLastPOWBlock = 210;
        nMaturity = 100;
        nCollateralMaturity = 24 * 60; //1 day
        nCollateralMaturityEnforcementHeight = 1000;
        nCollateralMaturityTimeWindow = 24 * 60; //1 day
        nMasternodeMaxCount = 10;		// maximum number of masternodes allowed
        nMasternodeCountDrift = 4;
        nModifierUpdateBlock = 1; //approx Mon, 17 Apr 2017 04:00:00 GMT
        nMaxMoneyOut = 92000000000 * COIN;
        nMasternodeCollateral = 7499; 
        nStakeMinConfirmations = 15;   // Required number of confirmations
        nStakeMinAmount = 10 * COIN;    // Minimum required staking amount


        genesis.nTime = 1598616000;
        genesis.nNonce = 2441669;

        hashGenesisBlock = genesis.GetHash();
        assert(hashGenesisBlock == uint256("0x000009997ec1c62ee220373ba6e563121dadd3e465cd01672a4312e4864fcc81"));

        vFixedSeeds.clear();
        vSeeds.clear();
        vSeeds.push_back(CDNSSeedData("seed1", "seed1.g999.io"));
        vSeeds.push_back(CDNSSeedData("seed2", "seed2.g999.io"));
        vSeeds.push_back(CDNSSeedData("seed3", "seed3.g999.io"));
        vSeeds.push_back(CDNSSeedData("seed4", "seed4.g999.io"));
        vSeeds.push_back(CDNSSeedData("seed5", "seed5.g999.io"));
        vSeeds.push_back(CDNSSeedData("seed6", "seed6.g999.io"));

        base58Prefixes[PUBKEY_ADDRESS] = std::vector<unsigned char>(1, 127); // Testnet G999 addresses start with 't'
        base58Prefixes[SCRIPT_ADDRESS] = std::vector<unsigned char>(1, 15);  // Testnet G999 script addresses start with '7'
        base58Prefixes[SECRET_KEY] = std::vector<unsigned char>(1, 239);     // Testnet private keys start with '9' or 'c' (Bitcoin defaults)
		// G999 BIP32 pubkeys start with 'xpub' (Bitcoin defaults)
        base58Prefixes[EXT_PUBLIC_KEY] = boost::assign::list_of(0x04)(0x88)(0xB2)(0x1E).convert_to_container<std::vector<unsigned char> >();
		// G999 BIP32 pubkeys start with 'xprv' (Bitcoin defaults)		
        base58Prefixes[EXT_SECRET_KEY] = boost::assign::list_of(0x04)(0x88)(0xAD)(0xE4).convert_to_container<std::vector<unsigned char> >();
        // Testnet G999 BIP44 coin type is '1' (All coin's testnet default)
        base58Prefixes[EXT_COIN_TYPE] = boost::assign::list_of(0x80)(0x00)(0x00)(0x01).convert_to_container<std::vector<unsigned char> >();

        convertSeed6(vFixedSeeds, pnSeed6_test, ARRAYLEN(pnSeed6_test));

        fRequireRPCPassword = true;
        fMiningRequiresPeers = true;
        fAllowMinDifficultyBlocks = true;
        fDefaultConsistencyChecks = false;
        fRequireStandard = false;
        fMineBlocksOnDemand = false;
        fTestnetToBeDeprecatedFieldRPC = true;

        nPoolMaxTransactions = 2;
        strSporkKey = "040bf79c321e4305913e8e1f89ee6046df974b7aeeaf56fe172dc6e6b4fa7de6cf54df8d9c410059497074c018a40515468b90e172b4f674b2ce999af4669c6a12";
        strMasternodesendPoolDummyAddress = "GVMymvtExa1qnQ7gZzuYG73R4hF421oQTG";
        nStartMasternodePayments = 1598616000;
    }
    const Checkpoints::CCheckpointData& Checkpoints() const
    {
        return dataTestnet;
    }
};
static CTestNetParams testNetParams;

/**
 * Regression test
 */
class CRegTestParams : public CTestNetParams
{
public:
    CRegTestParams()
    {
        networkID = CBaseChainParams::REGTEST;
        strNetworkID = "regtest";
        strNetworkID = "regtest";
        pchMessageStart[0] = 0x3c;
        pchMessageStart[1] = 0xfb;
        pchMessageStart[2] = 0x74;
        pchMessageStart[3] = 0x67;
        nSubsidyHalvingInterval = 150;
        nEnforceBlockUpgradeMajority = 750;
        nRejectBlockOutdatedMajority = 950;
        nToCheckBlockUpgradeMajority = 1000;
        nMinerThreads = 1;
        nTargetTimespan = 24 * 60 * 60; // G999: 1 day
        nTargetSpacing = 1 * 60;        // G999: 1 minutes
        bnProofOfWorkLimit = ~uint256(0) >> 1;
        genesis.nTime = 1598616000;
        genesis.nBits = 0x207fffff;
        genesis.nNonce = 2;

        hashGenesisBlock = genesis.GetHash();
        nDefaultPort = 33995;
        assert(hashGenesisBlock == uint256("0x3961302567a947685d9b6ab67107245002f78b1e5ad059c181a699366fbe5dc2"));

        vFixedSeeds.clear(); //! Testnet mode doesn't have any fixed seeds.
        vSeeds.clear();      //! Testnet mode doesn't have any DNS seeds.

        fRequireRPCPassword = false;
        fMiningRequiresPeers = false;
        fAllowMinDifficultyBlocks = true;
        fDefaultConsistencyChecks = true;
        fRequireStandard = false;
        fMineBlocksOnDemand = true;
        fTestnetToBeDeprecatedFieldRPC = false;
    }
    const Checkpoints::CCheckpointData& Checkpoints() const
    {
        return dataRegtest;
    }
};
static CRegTestParams regTestParams;

/**
 * Unit test
 */
class CUnitTestParams : public CMainParams, public CModifiableParams
{
public:
    CUnitTestParams()
    {
        networkID = CBaseChainParams::UNITTEST;
        strNetworkID = "unittest";
        nDefaultPort = 33993;
        vFixedSeeds.clear(); //! Unit test mode doesn't have any fixed seeds.
        vSeeds.clear();      //! Unit test mode doesn't have any DNS seeds.

        fRequireRPCPassword = false;
        fMiningRequiresPeers = false;
        fDefaultConsistencyChecks = true;
        fAllowMinDifficultyBlocks = false;
        fMineBlocksOnDemand = true;
    }

    const Checkpoints::CCheckpointData& Checkpoints() const
    {
        // UnitTest share the same checkpoints as MAIN
        return data;
    }

    //! Published setters to allow changing values in unit test cases
    virtual void setSubsidyHalvingInterval(int anSubsidyHalvingInterval) { nSubsidyHalvingInterval = anSubsidyHalvingInterval; }
    virtual void setEnforceBlockUpgradeMajority(int anEnforceBlockUpgradeMajority) { nEnforceBlockUpgradeMajority = anEnforceBlockUpgradeMajority; }
    virtual void setRejectBlockOutdatedMajority(int anRejectBlockOutdatedMajority) { nRejectBlockOutdatedMajority = anRejectBlockOutdatedMajority; }
    virtual void setToCheckBlockUpgradeMajority(int anToCheckBlockUpgradeMajority) { nToCheckBlockUpgradeMajority = anToCheckBlockUpgradeMajority; }
    virtual void setDefaultConsistencyChecks(bool afDefaultConsistencyChecks) { fDefaultConsistencyChecks = afDefaultConsistencyChecks; }
    virtual void setAllowMinDifficultyBlocks(bool afAllowMinDifficultyBlocks) { fAllowMinDifficultyBlocks = afAllowMinDifficultyBlocks; }
    virtual void setSkipProofOfWorkCheck(bool afSkipProofOfWorkCheck) { fSkipProofOfWorkCheck = afSkipProofOfWorkCheck; }
};
static CUnitTestParams unitTestParams;


static CChainParams* pCurrentParams = 0;

CModifiableParams* ModifiableParams()
{
    assert(pCurrentParams);
    assert(pCurrentParams == &unitTestParams);
    return (CModifiableParams*)&unitTestParams;
}

const CChainParams& Params()
{
    assert(pCurrentParams);
    return *pCurrentParams;
}

CChainParams& Params(CBaseChainParams::Network network)
{
    switch (network) {
    case CBaseChainParams::MAIN:
        return mainParams;
    case CBaseChainParams::TESTNET:
        return testNetParams;
    case CBaseChainParams::REGTEST:
        return regTestParams;
    case CBaseChainParams::UNITTEST:
        return unitTestParams;
    default:
        assert(false && "Unimplemented network");
        return mainParams;
    }
}

void SelectParams(CBaseChainParams::Network network)
{
    SelectBaseParams(network);
    pCurrentParams = &Params(network);
}

bool SelectParamsFromCommandLine()
{
    CBaseChainParams::Network network = NetworkIdFromCommandLine();
    if (network == CBaseChainParams::MAX_NETWORK_TYPES)
        return false;

    SelectParams(network);
    return true;
}

bool CChainParams::nonCollateralMaturity(int nTxHeight) const {

   int nDiv = nTxHeight / nCollateralMaturity;
   int nRem = nTxHeight % nCollateralMaturity;

   if(nDiv >= 1 && nRem <= nCollateralMaturityTimeWindow)
       return false;
   return true;
}
