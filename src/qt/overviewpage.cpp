// Copyright (c) 2011-2014 The Bitcoin developers
// Copyright (c) 2014-2015 The Dash developers
// Copyright (c) 2015-2018 The PIVX developers
// Copyright (c) 2018-2020 The G999 Core developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "overviewpage.h"
#include "ui_overviewpage.h"

#include "bitcoinunits.h"
#include "clientmodel.h"
#include "guiconstants.h"
#include "guiutil.h"
#include "init.h"
#include "masternodesend.h"
#include "masternodesendconfig.h"
#include "optionsmodel.h"
#include "transactionfilterproxy.h"
#include "transactiontablemodel.h"
#include "walletmodel.h"

#include <QAbstractItemDelegate>
#include <QPainter>
#include <QSettings>
#include <QTimer>

#define DECORATION_SIZE 48
#define ICON_OFFSET 16
#define NUM_ITEMS 20

class TxViewDelegate : public QAbstractItemDelegate
{
    Q_OBJECT
public:
    TxViewDelegate() : QAbstractItemDelegate(), unit(BitcoinUnits::G999)
    {
    }

    inline void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
    {
        painter->save();

        QIcon icon = qvariant_cast<QIcon>(index.data(Qt::DecorationRole));
        QRect mainRect = option.rect;
        mainRect.moveLeft(ICON_OFFSET);
        QRect decorationRect(mainRect.topLeft(), QSize(DECORATION_SIZE, DECORATION_SIZE));
        int xspace = DECORATION_SIZE + 8;
        int ypad = 6;
        int halfheight = (mainRect.height() - 2 * ypad) / 2;
        QRect amountRect(mainRect.left() + xspace, mainRect.top() + ypad, mainRect.width() - xspace - ICON_OFFSET, halfheight);
        QRect addressRect(mainRect.left() + xspace, mainRect.top() + ypad + halfheight, mainRect.width() - xspace, halfheight);
        icon.paint(painter, decorationRect);

        QDateTime date = index.data(TransactionTableModel::DateRole).toDateTime();
        QString address = index.data(Qt::DisplayRole).toString();
        qint64 amount = index.data(TransactionTableModel::AmountRole).toLongLong();
        bool confirmed = index.data(TransactionTableModel::ConfirmedRole).toBool();
        QVariant value = index.data(Qt::ForegroundRole);
        QColor foreground = COLOR_BLACK;
        if (value.canConvert<QBrush>()) {
            QBrush brush = qvariant_cast<QBrush>(value);
            foreground = brush.color();
        }

        painter->setPen(foreground);
        QRect boundingRect;
        painter->drawText(addressRect, Qt::AlignLeft | Qt::AlignVCenter, address, &boundingRect);

        if (index.data(TransactionTableModel::WatchonlyRole).toBool()) {
            QIcon iconWatchonly = qvariant_cast<QIcon>(index.data(TransactionTableModel::WatchonlyDecorationRole));
            QRect watchonlyRect(boundingRect.right() + 5, mainRect.top() + ypad + halfheight, 16, halfheight);
            iconWatchonly.paint(painter, watchonlyRect);
        }

        if (amount < 0) {
            foreground = COLOR_NEGATIVE;
        } else if (!confirmed) {
            foreground = COLOR_UNCONFIRMED;
        } else {
            foreground = COLOR_BLACK;
        }
        painter->setPen(foreground);
        QString amountText = BitcoinUnits::formatWithUnit(unit, amount, true, BitcoinUnits::separatorAlways);
        if (!confirmed) {
            amountText = QString("[") + amountText + QString("]");
        }
        painter->drawText(amountRect, Qt::AlignRight | Qt::AlignVCenter, amountText);

        painter->setPen(COLOR_BLACK);
        painter->drawText(amountRect, Qt::AlignLeft | Qt::AlignVCenter, GUIUtil::dateTimeStr(date));

        painter->restore();
    }

    inline QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
    {
        return QSize(DECORATION_SIZE, DECORATION_SIZE);
    }

    int unit;
};
#include "overviewpage.moc"

OverviewPage::OverviewPage(QWidget* parent) : QWidget(parent),
                                              ui(new Ui::OverviewPage),
                                              clientModel(0),
                                              walletModel(0),
                                              currentBalance(-1),
                                              currentUnconfirmedBalance(-1),
                                              currentImmatureBalance(-1),
                                              currentWatchOnlyBalance(-1),
                                              currentWatchUnconfBalance(-1),
                                              currentWatchImmatureBalance(-1),
                                              txdelegate(new TxViewDelegate()),
                                              filter(0)
{
    nDisplayUnit = 0; // just make sure it's not unitialized
    ui->setupUi(this);
    
    ui->listTransactions->setShowGrid(false);
    ui->listTransactions->setFrameStyle(QFrame::NoFrame);
    ui->listTransactions->setStyleSheet("QTableWidget {border: none;}");

    // Recent transactions
    int columnDateWidth = 150;
    int columnTypeWidth = 150;    
    int columnAmountWidth = 150;

    ui->listTransactions->setColumnWidth(0, columnDateWidth);
    ui->listTransactions->setColumnWidth(1, columnTypeWidth);
    ui->listTransactions->setColumnWidth(3, columnAmountWidth);
    
    
// Set column widths
#if QT_VERSION < 0x050000
    ui->listTransactions->horizontalHeader()->setResizeMode(2, QHeaderView::Stretch);
#else
    ui->listTransactions->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
#endif

    ui->listTransactions->horizontalHeaderItem(0)->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    ui->listTransactions->horizontalHeaderItem(1)->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    ui->listTransactions->horizontalHeaderItem(2)->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    ui->listTransactions->horizontalHeaderItem(3)->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    ui->listTransactions->setEditTriggers(QAbstractItemView::NoEditTriggers);
    connect(ui->listTransactions, SIGNAL(clicked(QModelIndex)), this, SLOT(handleTransactionClicked(QModelIndex)));
    
    columnResizingFixer = new GUIUtil::RecentTableViewLastColumnResizingFixer(ui->listTransactions, 120, 23);

    // init "out of sync" warning labels
    ui->labelWalletStatus->setVisible(false);
    ui->labelMasternodesendSyncStatus->setText("(" + tr("out of sync") + ")");
    ui->labelTransactionsStatus->setText( tr("OUT OF SYNC") );

    if (fLiteMode || fDisableMasternodeSend) {
        ui->frameMasternodesend->setVisible(false);
    } else {
        if (fMasterNode) {
            ui->toggleMasternodesend->setText("(" + tr("Disabled") + ")");
            ui->masternodesendAuto->setText("(" + tr("Disabled") + ")");
            ui->masternodesendReset->setText("(" + tr("Disabled") + ")");
            ui->frameMasternodesend->setEnabled(false);
        } else {
            if (!fEnableMasternodesend) {
                ui->toggleMasternodesend->setText(tr("Start Masternodesend"));
            } else {
                ui->toggleMasternodesend->setText(tr("Stop Masternodesend"));
            }
            timer = new QTimer(this);
            connect(timer, SIGNAL(timeout()), this, SLOT(masternodeSendStatus()));
            timer->start(1000);
        }
    }

    // start with displaying the "out of sync" warnings
    showOutOfSyncWarning(true);

    // refresh recent list with a timer while out of sync, then on every setBalance signal
    recentlistTimer = new QTimer(this);
    connect(recentlistTimer, SIGNAL(timeout()), this, SLOT(updateTransactionList()));
    if (!masternodeSync.IsBlockchainSynced())    {
        recentlistTimer->start(10000);
    }
}

void OverviewPage::on_resizeMainWindow(QResizeEvent* event)
{
	columnResizingFixer->stretchColumnWidth(2, event->size().width());
}

void OverviewPage::handleTransactionClicked(const QModelIndex& index)
{       
    // re-map to transaction view source index 
    int nSelectedRow = index.row();
    QTableWidgetItem* dateItem = ui->listTransactions->item(nSelectedRow, 0);
    int listIndex = dateItem->data(Qt::UserRole).toInt();    
    TransactionTableModel *ttm = walletModel->getTransactionTableModel();
    QModelIndex rowIndex = ttm->index(listIndex, TransactionTableModel::Date, QModelIndex() );    // any role is ok
        
    emit transactionClicked(rowIndex);
}

OverviewPage::~OverviewPage()
{
    if (!fLiteMode && !fMasterNode && !fDisableMasternodeSend) disconnect(timer, SIGNAL(timeout()), this, SLOT(masternodeSendStatus()));
    delete ui;
}

void OverviewPage::setBalance(const CAmount& balance, const CAmount& unconfirmedBalance, const CAmount& immatureBalance, const CAmount& anonymizedBalance, const CAmount& watchOnlyBalance, const CAmount& watchUnconfBalance, const CAmount& watchImmatureBalance)
{
    currentBalance = balance;
    currentUnconfirmedBalance = unconfirmedBalance;
    currentImmatureBalance = immatureBalance;
    currentAnonymizedBalance = anonymizedBalance;
    currentWatchOnlyBalance = watchOnlyBalance;
    currentWatchUnconfBalance = watchUnconfBalance;
    currentWatchImmatureBalance = watchImmatureBalance;

    ui->labelBalance->setText(BitcoinUnits::floorHtmlWithoutUnit(nDisplayUnit, balance - immatureBalance, false, BitcoinUnits::separatorAlways)+getTextLabelHTML(nDisplayUnit, tr("AVAILABLE")));
    ui->labelUnconfirmed->setText(BitcoinUnits::floorHtmlWithoutUnit(nDisplayUnit, unconfirmedBalance, false, BitcoinUnits::separatorAlways)+getTextLabelHTML(nDisplayUnit, tr("PENDING")));
    ui->labelImmature->setText(BitcoinUnits::floorHtmlWithoutUnit(nDisplayUnit, immatureBalance, false, BitcoinUnits::separatorAlways)+getTextLabelHTML(nDisplayUnit, tr("IMMATURE")));
    ui->labelAnonymized->setText(BitcoinUnits::floorHtmlWithoutUnit(nDisplayUnit, anonymizedBalance, false, BitcoinUnits::separatorAlways));
    ui->labelTotal->setText(BitcoinUnits::floorHtmlWithoutUnit(nDisplayUnit, balance + unconfirmedBalance, false, BitcoinUnits::separatorAlways)+getTextLabelHTML(nDisplayUnit, tr("TOTAL")));


    ui->labelWatchAvailable->setText(BitcoinUnits::floorHtmlWithoutUnit(nDisplayUnit, watchOnlyBalance, false, BitcoinUnits::separatorAlways)+getTextLabelHTML(nDisplayUnit, tr("AVAILABLE")+" "+tr("(Watch Only)")));
    ui->labelWatchPending->setText(BitcoinUnits::floorHtmlWithoutUnit(nDisplayUnit, watchUnconfBalance, false, BitcoinUnits::separatorAlways)+getTextLabelHTML(nDisplayUnit, tr("PENDING")+" "+tr("(Watch Only)")));
    ui->labelWatchImmature->setText(BitcoinUnits::floorHtmlWithoutUnit(nDisplayUnit, watchImmatureBalance, false, BitcoinUnits::separatorAlways)+getTextLabelHTML(nDisplayUnit, tr("IMMATURE")+" "+tr("(Watch Only)")));
    ui->labelWatchTotal->setText(BitcoinUnits::floorHtmlWithoutUnit(nDisplayUnit, watchOnlyBalance + watchUnconfBalance + watchImmatureBalance, false, BitcoinUnits::separatorAlways)+getTextLabelHTML(nDisplayUnit, tr("TOTAL")+" "+tr("(Watch Only)")));

    // only show immature (newly mined) balance if it's non-zero, so as not to complicate things
    // for the non-mining users
    bool showImmature = immatureBalance != 0;
    bool showWatchOnlyImmature = (watchImmatureBalance != 0) || (showImmature && walletModel->haveWatchOnly() );

    // for symmetry reasons also show immature label when the watch-only one is shown
    ui->labelImmature->setVisible(showImmature || showWatchOnlyImmature);
    //ui->labelImmatureText->setVisible(showImmature || showWatchOnlyImmature);
    //ui->frame_5->setVisible(showImmature || showWatchOnlyImmature);
    ui->labelWatchImmature->setVisible(showWatchOnlyImmature); // show watch-only immature balance
    //ui->labelWatchImmatureText->setVisible(showWatchOnlyImmature);
    //ui->frame_9->setVisible(showWatchOnlyImmature);

    updateMasternodesendProgress();

    static int cachedTxLocks = 0;

    if (masternodeSync.IsBlockchainSynced())    {
        updateTransactionList();    
    }

    if (cachedTxLocks != nCompleteTXLocks) {
        cachedTxLocks = nCompleteTXLocks;
    }
}

void OverviewPage::updateTransactionList()
{
    ui->listTransactions->setSortingEnabled(false);
    ui->listTransactions->clearContents();
    ui->listTransactions->setRowCount(0);
    
    TransactionTableModel *ttm = walletModel->getTransactionTableModel();
    int totalRows = ttm->rowCount( QModelIndex() );
    int i, n = 0;

    QMap<QVariant, int> orderedMap; 
    for ( i=0;i<totalRows;i++) {
        QModelIndex dateMIndex = ttm->index(i, TransactionTableModel::Date, QModelIndex() );            
        QVariant date = ttm->data(dateMIndex, TransactionTableModel::DateRole);
        orderedMap[date] = i;
    }

    QMapIterator<QVariant, int> it(orderedMap);
    it.toBack();
    while (it.hasPrevious() && n<NUM_ITEMS) {
        it.previous();
        i = it.value();
        QVariant date = it.key();
        qint64 amount = ttm->index(i, TransactionTableModel::Amount, QModelIndex()).data(Qt::EditRole).toULongLong();
        QString strAmount = BitcoinUnits::format(walletModel->getOptionsModel()->getDisplayUnit(), amount, false, BitcoinUnits::separatorAlways);    
        QString type = ttm->index(i, TransactionTableModel::Type, QModelIndex()).data().toString();
        QString address = ttm->index(i, TransactionTableModel::ToAddress, QModelIndex()).data().toString();

        // DATE, type, Address,amount
        QTableWidgetItem* dateItem = new QTableWidgetItem();
        dateItem->setData(Qt::DisplayRole, date);
        dateItem->setData(Qt::UserRole, i);        // store the original index in datasource to handle the click
        QTableWidgetItem* typeItem = new QTableWidgetItem(type);
        QTableWidgetItem* addressItem = new QTableWidgetItem(address);
        QTableWidgetItem* amountItem = new QTableWidgetItem(strAmount);

        ui->listTransactions->insertRow(0);
        ui->listTransactions->setItem(0, 0, dateItem);
        ui->listTransactions->setItem(0, 1, typeItem);
        ui->listTransactions->setItem(0, 2, addressItem);
        ui->listTransactions->setItem(0, 3, amountItem);

        n++;
    }

    ui->listTransactions->sortByColumn(0,Qt::DescendingOrder);
    ui->listTransactions->setSortingEnabled(true);

    // in case we have 0 balance and setBalance is never called, we stop the timer here
    if (masternodeSync.IsBlockchainSynced())    {
        if ( recentlistTimer!=NULL && recentlistTimer->isActive() )  {
            recentlistTimer->stop();
        }
    }
}

QString OverviewPage::getTextLabelHTML( int nDisplayUnit, QString labeltxt )    {
    return "<br><span style=\"font-size:10pt;color:#5e5e5e;\">"+labeltxt+" "+BitcoinUnits::name(nDisplayUnit)+"</span>";
}

// show/hide watch-only labels
void OverviewPage::updateWatchOnlyLabels(bool showWatchOnly)
{
    //ui->labelSpendable->setVisible(showWatchOnly);      // show spendable label (only when watch-only is active)
    //ui->labelWatchonly->setVisible(showWatchOnly);      // show watch-only label
    //ui->lineWatchBalance->setVisible(showWatchOnly);    // show watch-only balance separator line
    ui->labelWatchAvailable->setVisible(showWatchOnly); // show watch-only available balance
    ui->labelWatchPending->setVisible(showWatchOnly);   // show watch-only pending balance
    ui->labelWatchTotal->setVisible(showWatchOnly);     // show watch-only total balance
    //ui->labelWatchAvailableText->setVisible(showWatchOnly); // show watch-only available balance
    //ui->labelWatchPendingText->setVisible(showWatchOnly);   // show watch-only pending balance
    //ui->labelWatchTotalText->setVisible(showWatchOnly);     // show watch-only total balance
    //ui->frame_7->setVisible(showWatchOnly);
    //ui->frame_8->setVisible(showWatchOnly);
    //ui->frame_10->setVisible(showWatchOnly);

    if (!showWatchOnly) {
        ui->labelWatchImmature->hide();
    } else {
        ui->labelBalance->setIndent(20);
        ui->labelUnconfirmed->setIndent(20);
        ui->labelImmature->setIndent(20);
        ui->labelTotal->setIndent(20);
    }
}

void OverviewPage::setClientModel(ClientModel* model)
{
    this->clientModel = model;
    if (model) {
        // Show warning if this is a prerelease version
        connect(model, SIGNAL(alertsChanged(QString)), this, SLOT(updateAlerts(QString)));
        updateAlerts(model->getStatusBarWarnings());
    }
}

void OverviewPage::setWalletModel(WalletModel* model)
{
    this->walletModel = model;
    if (model && model->getOptionsModel()) {
        // Keep up to date with wallet
        setBalance(model->getBalance(), model->getUnconfirmedBalance(), model->getImmatureBalance(), model->getAnonymizedBalance(),
            model->getWatchBalance(), model->getWatchUnconfirmedBalance(), model->getWatchImmatureBalance());
        connect(model, SIGNAL(balanceChanged(CAmount, CAmount, CAmount, CAmount, CAmount, CAmount, CAmount)), this, SLOT(setBalance(CAmount, CAmount, CAmount, CAmount, CAmount, CAmount, CAmount)));

        connect(model->getOptionsModel(), SIGNAL(displayUnitChanged(int)), this, SLOT(updateDisplayUnit()));

        connect(ui->masternodesendAuto, SIGNAL(clicked()), this, SLOT(masternodesendAuto()));
        connect(ui->masternodesendReset, SIGNAL(clicked()), this, SLOT(masternodesendReset()));
        connect(ui->toggleMasternodesend, SIGNAL(clicked()), this, SLOT(toggleMasternodesend()));
        updateWatchOnlyLabels( model->haveWatchOnly() );
        connect(model, SIGNAL(notifyWatchonlyChanged(bool)), this, SLOT(updateWatchOnlyLabels(bool)));
    }

    // update the display unit, to not use the default ("G999")
    updateDisplayUnit();
}

void OverviewPage::updateDisplayUnit()
{
    if (walletModel && walletModel->getOptionsModel()) {
        nDisplayUnit = walletModel->getOptionsModel()->getDisplayUnit();
        if (currentBalance != -1)
            setBalance(currentBalance, currentUnconfirmedBalance, currentImmatureBalance, currentAnonymizedBalance,
                currentWatchOnlyBalance, currentWatchUnconfBalance, currentWatchImmatureBalance);

        // Update txdelegate->unit with the current unit
        txdelegate->unit = nDisplayUnit;

        updateTransactionList();
    }
}

void OverviewPage::updateAlerts(const QString& warnings)
{
    this->ui->labelAlerts->setVisible(!warnings.isEmpty());
    this->ui->labelAlerts->setText(warnings);
}

void OverviewPage::showOutOfSyncWarning(bool fShow)
{
    ui->labelMasternodesendSyncStatus->setVisible(fShow);
    ui->labelTransactionsStatus->setVisible(fShow);
}

void OverviewPage::updateMasternodesendProgress()
{
    if (!masternodeSync.IsBlockchainSynced() || ShutdownRequested()) return;

    if (!pwalletMain) return;

    QString strAmountAndRounds;
    QString strAnonymizeG999Amount = BitcoinUnits::formatHtmlWithUnit(nDisplayUnit, nAnonymizeG999Amount * COIN, false, BitcoinUnits::separatorAlways);

    if (currentBalance == 0) {
        ui->masternodesendProgress->setValue(0);
        ui->masternodesendProgress->setToolTip(tr("No inputs detected"));

        // when balance is zero just show info from settings
        strAnonymizeG999Amount = strAnonymizeG999Amount.remove(strAnonymizeG999Amount.indexOf("."), BitcoinUnits::decimals(nDisplayUnit) + 1);
        strAmountAndRounds = strAnonymizeG999Amount + " / " + tr("%n Rounds", "", nMasternodesendRounds);

        ui->labelAmountRounds->setToolTip(tr("No inputs detected"));
        ui->labelAmountRounds->setText(strAmountAndRounds);
        return;
    }

    CAmount nDenominatedConfirmedBalance;
    CAmount nDenominatedUnconfirmedBalance;
    CAmount nAnonymizableBalance;
    CAmount nNormalizedAnonymizedBalance;
    double nAverageAnonymizedRounds;

    {
        TRY_LOCK(cs_main, lockMain);
        if (!lockMain) return;

        nDenominatedConfirmedBalance = pwalletMain->GetDenominatedBalance();
        nDenominatedUnconfirmedBalance = pwalletMain->GetDenominatedBalance(true);
        nAnonymizableBalance = pwalletMain->GetAnonymizableBalance();
        nNormalizedAnonymizedBalance = pwalletMain->GetNormalizedAnonymizedBalance();
        nAverageAnonymizedRounds = pwalletMain->GetAverageAnonymizedRounds();
    }

    CAmount nMaxToAnonymize = nAnonymizableBalance + currentAnonymizedBalance + nDenominatedUnconfirmedBalance;

    // If it's more than the anon threshold, limit to that.
    if (nMaxToAnonymize > nAnonymizeG999Amount * COIN) nMaxToAnonymize = nAnonymizeG999Amount * COIN;

    if (nMaxToAnonymize == 0) return;

    if (nMaxToAnonymize >= nAnonymizeG999Amount * COIN) {
        ui->labelAmountRounds->setToolTip(tr("Found enough compatible inputs to anonymize %1")
                                              .arg(strAnonymizeG999Amount));
        strAnonymizeG999Amount = strAnonymizeG999Amount.remove(strAnonymizeG999Amount.indexOf("."), BitcoinUnits::decimals(nDisplayUnit) + 1);
        strAmountAndRounds = strAnonymizeG999Amount + " / " + tr("%n Rounds", "", nMasternodesendRounds);
    } else {
        QString strMaxToAnonymize = BitcoinUnits::formatHtmlWithUnit(nDisplayUnit, nMaxToAnonymize, false, BitcoinUnits::separatorAlways);
        ui->labelAmountRounds->setToolTip(tr("Not enough compatible inputs to anonymize <span style='color:red;'>%1</span>,<br>"
                                             "will anonymize <span style='color:red;'>%2</span> instead")
                                              .arg(strAnonymizeG999Amount)
                                              .arg(strMaxToAnonymize));
        strMaxToAnonymize = strMaxToAnonymize.remove(strMaxToAnonymize.indexOf("."), BitcoinUnits::decimals(nDisplayUnit) + 1);
        strAmountAndRounds = "<span style='color:red;'>" +
                             QString(BitcoinUnits::factor(nDisplayUnit) == 1 ? "" : "~") + strMaxToAnonymize +
                             " / " + tr("%n Rounds", "", nMasternodesendRounds) + "</span>";
    }
    ui->labelAmountRounds->setText(strAmountAndRounds);

    // calculate parts of the progress, each of them shouldn't be higher than 1
    // progress of denominating
    float denomPart = 0;
    // mixing progress of denominated balance
    float anonNormPart = 0;
    // completeness of full amount anonimization
    float anonFullPart = 0;

    CAmount denominatedBalance = nDenominatedConfirmedBalance + nDenominatedUnconfirmedBalance;
    denomPart = (float)denominatedBalance / nMaxToAnonymize;
    denomPart = denomPart > 1 ? 1 : denomPart;
    denomPart *= 100;

    anonNormPart = (float)nNormalizedAnonymizedBalance / nMaxToAnonymize;
    anonNormPart = anonNormPart > 1 ? 1 : anonNormPart;
    anonNormPart *= 100;

    anonFullPart = (float)currentAnonymizedBalance / nMaxToAnonymize;
    anonFullPart = anonFullPart > 1 ? 1 : anonFullPart;
    anonFullPart *= 100;

    // apply some weights to them ...
    float denomWeight = 1;
    float anonNormWeight = nMasternodesendRounds;
    float anonFullWeight = 2;
    float fullWeight = denomWeight + anonNormWeight + anonFullWeight;
    // ... and calculate the whole progress
    float denomPartCalc = ceilf((denomPart * denomWeight / fullWeight) * 100) / 100;
    float anonNormPartCalc = ceilf((anonNormPart * anonNormWeight / fullWeight) * 100) / 100;
    float anonFullPartCalc = ceilf((anonFullPart * anonFullWeight / fullWeight) * 100) / 100;
    float progress = denomPartCalc + anonNormPartCalc + anonFullPartCalc;
    if (progress >= 100) progress = 100;

    ui->masternodesendProgress->setValue(progress);

    QString strToolPip = ("<b>" + tr("Overall progress") + ": %1%</b><br/>" +
                          tr("Denominated") + ": %2%<br/>" +
                          tr("Mixed") + ": %3%<br/>" +
                          tr("Anonymized") + ": %4%<br/>" +
                          tr("Denominated inputs have %5 of %n rounds on average", "", nMasternodesendRounds))
                             .arg(progress)
                             .arg(denomPart)
                             .arg(anonNormPart)
                             .arg(anonFullPart)
                             .arg(nAverageAnonymizedRounds);
    ui->masternodesendProgress->setToolTip(strToolPip);
}


void OverviewPage::masternodeSendStatus()
{
    static int64_t nLastDSProgressBlockTime = 0;

    int nBestHeight = chainActive.Tip()->nHeight;

    // we we're processing more then 1 block per second, we'll just leave
    if (((nBestHeight - masternodeSendPool.cachedNumBlocks) / (GetTimeMillis() - nLastDSProgressBlockTime + 1) > 1)) return;
    nLastDSProgressBlockTime = GetTimeMillis();

    if (!fEnableMasternodesend) {
        if (nBestHeight != masternodeSendPool.cachedNumBlocks) {
            masternodeSendPool.cachedNumBlocks = nBestHeight;
            updateMasternodesendProgress();

            ui->masternodesendEnabled->setText(tr("Disabled"));
            ui->masternodesendStatus->setText("");
            ui->toggleMasternodesend->setText(tr("Start Masternodesend"));
        }

        return;
    }

    // check masternodesend status and unlock if needed
    if (nBestHeight != masternodeSendPool.cachedNumBlocks) {
        // Balance and number of transactions might have changed
        masternodeSendPool.cachedNumBlocks = nBestHeight;
        updateMasternodesendProgress();

        ui->masternodesendEnabled->setText(tr("Enabled"));
    }

    QString strStatus = QString(masternodeSendPool.GetStatus().c_str());

    QString s = tr("Last Masternodesend message:\n") + strStatus;

    if (s != ui->masternodesendStatus->text())
        LogPrintf("Last Masternodesend message: %s\n", strStatus.toStdString());

    ui->masternodesendStatus->setText(s);

    if (masternodeSendPool.sessionDenom == 0) {
        ui->labelSubmittedDenom->setText(tr("N/A"));
    } else {
        std::string out;
        masternodeSendPool.GetDenominationsToString(masternodeSendPool.sessionDenom, out);
        QString s2(out.c_str());
        ui->labelSubmittedDenom->setText(s2);
    }
}

void OverviewPage::masternodesendAuto()
{
    masternodeSendPool.DoAutomaticDenominating();
}

void OverviewPage::masternodesendReset()
{
    masternodeSendPool.Reset();

    QMessageBox::warning(this, tr("Masternodesend"),
        tr("Masternodesend was successfully reset."),
        QMessageBox::Ok, QMessageBox::Ok);
}

void OverviewPage::toggleMasternodesend()
{
    QSettings settings;
    // Popup some information on first mixing
    QString hasMixed = settings.value("hasMixed").toString();
    if (hasMixed.isEmpty()) {
        QMessageBox::information(this, tr("Masternodesend"),
            tr("If you don't want to see internal Masternodesend fees/transactions select \"Most Common\" as Type on the \"Transactions\" tab."),
            QMessageBox::Ok, QMessageBox::Ok);
        settings.setValue("hasMixed", "hasMixed");
    }
    if (!fEnableMasternodesend) {
        CAmount balance = currentBalance;
        float minAmount = 14.90 * COIN;
        if (balance < minAmount) {
            QString strMinAmount(BitcoinUnits::formatWithUnit(nDisplayUnit, minAmount));
            QMessageBox::warning(this, tr("Masternodesend"),
                tr("Masternodesend requires at least %1 to use.").arg(strMinAmount),
                QMessageBox::Ok, QMessageBox::Ok);
            return;
        }

        // if wallet is locked, ask for a passphrase
        if (walletModel->getEncryptionStatus() == WalletModel::Locked) {
            WalletModel::UnlockContext ctx(walletModel->requestUnlock(false));
            if (!ctx.isValid()) {
                //unlock was cancelled
                masternodeSendPool.cachedNumBlocks = std::numeric_limits<int>::max();
                QMessageBox::warning(this, tr("Masternodesend"),
                    tr("Wallet is locked and user declined to unlock. Disabling Masternodesend."),
                    QMessageBox::Ok, QMessageBox::Ok);
                if (fDebug) LogPrintf("Wallet is locked and user declined to unlock. Disabling Masternodesend.\n");
                return;
            }
        }
    }

    fEnableMasternodesend = !fEnableMasternodesend;
    masternodeSendPool.cachedNumBlocks = std::numeric_limits<int>::max();

    if (!fEnableMasternodesend) {
        ui->toggleMasternodesend->setText(tr("Start Masternodesend"));
        masternodeSendPool.UnlockCoins();
    } else {
        ui->toggleMasternodesend->setText(tr("Stop Masternodesend"));

        /* show masternodesend configuration if client has defaults set */

        if (nAnonymizeG999Amount == 0) {
            MasternodesendConfig dlg(this);
            dlg.setModel(walletModel);
            dlg.exec();
        }
    }
}

void OverviewPage::paintEvent(QPaintEvent *p2)
{
    QPixmap pixmap;

    pixmap.load(":/res/images/bg_lion.png");

    QPainter paint(this);
    paint.drawPixmap(0, 0, pixmap);
    QWidget::paintEvent(p2);
}